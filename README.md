# TSL Tech Assignment

## Prerequisites

This project requires NPM and Ionic 4

On Mac/Linux, run the following command to install Ionic 4

```
sudo npm i -g ionic
```


## Installing and Running

Be sure to first have the [backend application up and running](https://gitlab.com/cnsimbe/tsl-backend)

Clone the repository

```
git clone https://gitlab.com/cnsimbe/tsl-frontend.git
```


And then install the node modules, and run the web application

```
cd tsl-frontend
npm install
ionic serve
```


## Testing

```
npm run test
```


## Production Build

```
ionic build --prod
```


## Built With

* [Angular](https://angular.io/) - Angular v7
* [Ionic](http://beta.ionicframework.com/) - Cross platform & progressive web application framework