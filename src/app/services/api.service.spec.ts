import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ApiService } from './api.service';
import { httpInterceptorProviders } from "./http-interceptor"
import { Observable } from "rxjs"
import { User, WallMessage } from "../models"

describe('ApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({
  	imports: [HttpClientTestingModule],
  	providers: [ httpInterceptorProviders ]
  }));

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('should login user', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service.login('username','password')).toEqual(jasmine.any(Observable))
  });

  it('should create user', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service.register(new User)).toEqual(jasmine.any(Observable))
  });

  it('should get wallMessages', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service.getWallMessages()).toEqual(jasmine.any(Observable))
  });


  it('should create wall messages', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service.createMessage(new WallMessage)).toEqual(jasmine.any(Observable))
  });

});