import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Router } from "@angular/router"

import { Observable, of, throwError } from 'rxjs';
import { mergeMap, tap, catchError } from 'rxjs/operators'
import { environment } from "../../environments/environment"
import { AlertController } from "@ionic/angular"

const authURL = `${environment.apiServer}/user/auth`
const checkAuthURL = `${environment.apiServer}/user/profile`

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router:Router, private alertCtrl:AlertController){}
  private token:string;

  intercept(req: HttpRequest<any>, next: HttpHandler):
    Observable<HttpEvent<any>> {

    let token = this.getAuthToken();
    	let authReq = token ? req.clone({ setHeaders: { Authorization: `Token ${token}` } }) : req;
    return next.handle(authReq).pipe(tap((httpEvent:HttpResponse<any>)=>{
      
      let isAuthURL = httpEvent instanceof HttpResponse ? httpEvent.url == authURL : false;

      if(isAuthURL  && req.method.toLowerCase() == 'post')
    	  this.setToken(httpEvent.body.token)

      else if(isAuthURL && req.method.toLowerCase() == 'delete')
        this.deleteToken()

    }),catchError(error=>{

      let errorMessage = error instanceof Error ? error.message : undefined
      if(error instanceof HttpErrorResponse)
        errorMessage = typeof error.error == 'object' ? JSON.stringify(error.error) :  error.error

      let isCheckAuthURL = error instanceof HttpErrorResponse ? error.url == checkAuthURL : false;
      if(!isCheckAuthURL)
        this.alertCtrl.create({header:"Ooops something's wrong", message:errorMessage})
        .then(alert=>alert.present())

      if(error instanceof HttpErrorResponse && error.status == 401)
        this.deleteToken()

      return throwError(error);
    }))
  }

  getAuthToken():string {
    if(this.token === undefined)
      this.token = localStorage.getItem("auth-token") || null;
  	return this.token;
  }

  setToken(token) {
    this.token = token;
    localStorage.setItem("auth-token",token);
  }

  deleteToken() {
    this.token = null;
    localStorage.removeItem("auth-token")
  }
}

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
];
