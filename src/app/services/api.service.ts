import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http"
import { Observable, of } from "rxjs"
import { tap } from "rxjs/operators"
import { User, WallMessage, WallMessagePaginated } from "../models"
import { environment } from "../../environments/environment"

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  user:User;

  constructor(private http:HttpClient) { }

  login(username:string,password:string):Observable<User> {
  	return this.http.post<any>(`${environment.apiServer}/user/auth`,{username,password})
  	.pipe(tap(user=>this.user=user))
  }

  logout():Observable<{success:boolean}> {
  	return this.http.delete<any>(`${environment.apiServer}/user/auth`)
  	.pipe(tap(user=>this.user=undefined))
  }

  register(user:User):Observable<{success:boolean}> {
    return this.http.post<any>(`${environment.apiServer}/user/register`,user)
  }

  getProfile():Observable<User> {
  	return this.http.get<any>(`${environment.apiServer}/user/profile`)
  	.pipe(tap(user=>this.user=user))
  }

  createMessage(wallMessage:WallMessage):Observable<WallMessage> {
  	let formData = new FormData();
  	for(let key in wallMessage)
  		formData.append(key,wallMessage[key])
  	return this.http.post<any>(`${environment.apiServer}/wall/messages`,formData)
  }

  getWallMessages():Observable<WallMessagePaginated> {
  	return this.http.get<any>(`${environment.apiServer}/wall/messages`)
  }
  
  getWallPageMessages(url:string):Observable<WallMessagePaginated> {
  	return this.http.get<any>(url)
  }

}
