declare var jasmine;

import { Observable, of } from "rxjs"

let ApiServiceMock = jasmine.createSpyObj('ApiService', ['login', 'logout', 'register', 'getProfile', 'createMessage', 'getWallMessages', 'getWallPageMessages'] );

let wallMessages = { results : [] }

ApiServiceMock.login.and.returnValue(of({}));
ApiServiceMock.logout.and.returnValue(of({}));
ApiServiceMock.register.and.returnValue(of({}));
ApiServiceMock.getProfile.and.returnValue(of({}));
ApiServiceMock.createMessage.and.returnValue(of({}));
ApiServiceMock.getWallMessages.and.returnValue(of(wallMessages));
ApiServiceMock.getWallPageMessages.and.returnValue(of(wallMessages));

export { ApiServiceMock }