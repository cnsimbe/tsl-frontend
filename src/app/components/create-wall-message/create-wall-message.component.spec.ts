import { By } from '@angular/platform-browser';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CreateWallMessageComponent } from './create-wall-message.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ApiServiceMock } from "../../services/api.service.mock"
import { ApiService } from "../../services/api.service"
import { AlertControllerSpy, LoadingControllerSpy } from "../../tests-ionic-mocks/mock-controllers"
import { CommonModule } from '@angular/common';
import { IonicModule, AlertController, LoadingController } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ImageCropperModule } from 'ngx-image-cropper';
import { of } from "rxjs"
import { delay } from "rxjs/operators"
import { HttpClient, HttpHandler } from "@angular/common/http"


describe('CreateWallMessageComponent', () => {
  let component: CreateWallMessageComponent;
  let fixture: ComponentFixture<CreateWallMessageComponent>;
  let parentElement: HTMLElement;
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        IonicModule,
        FormsModule,
        ImageCropperModule,
        RouterTestingModule,
        HttpClientTestingModule,
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      declarations: [ CreateWallMessageComponent ],
      providers: [  { provide: ApiService, useValue:ApiServiceMock }, { provide: AlertController, useValue: AlertControllerSpy }, { provide: LoadingController, useValue: LoadingControllerSpy } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateWallMessageComponent);
    component = fixture.componentInstance;
    component.onDone = ()=>console.log("Wall Create")
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should not create wall message', async() => {
      spyOn(component,'onDone');

      await component.onSubmit()

      expect(component.onDone).not.toHaveBeenCalled()
      
  });


  it('should create wall message', async() => {

      component.message.text_message = "Hello World"
      spyOn(component,'onDone');
      
      await component.onSubmit()
      
      expect(component.onDone).toHaveBeenCalled()
      
  });


  it('should create wall message', async() => {
      let imageBlob = await fetch(new Request('https://picsum.photos/400/400')).then(response=>response.blob())
      component.inputFile = new File([imageBlob],"image.png")
      let croppedEvent = { file : imageBlob }
      component.imageCropped(croppedEvent as any)
      
      expect(component.message.picture).toBeTruthy()

      spyOn(component,'onDone');
      
      await component.onSubmit()


      expect(component.onDone).toHaveBeenCalled()
      
  });


  it('should call remove image handler', async() => {
      
      spyOn(component, 'removeImage')

      let removeImageBtn =  fixture.debugElement.query(By.css('[remove-image]'))
      removeImageBtn.triggerEventHandler('click',null)
      await fixture.whenRenderingDone()
      await fixture.whenStable()

      expect(component.removeImage).toHaveBeenCalled()
      
  });


  it('should remove image', async() => {
      let imageBlob = await fetch(new Request('https://picsum.photos/400/400')).then(response=>response.blob())
      component.inputFile = new File([imageBlob],"image.png")
      let croppedEvent = { file : imageBlob }
      component.imageCropped(croppedEvent as any)
      
      expect(component.message.picture).toBeTruthy()

      component.removeImage()

      expect(component.message.picture).toBeFalsy()
      
  });


});