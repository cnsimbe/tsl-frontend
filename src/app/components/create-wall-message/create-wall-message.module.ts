import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ImageCropperModule } from 'ngx-image-cropper';
import { CreateWallMessageComponent } from './create-wall-message.component';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImageCropperModule
  ],
  declarations: [CreateWallMessageComponent],
  entryComponents:[CreateWallMessageComponent],
  exports: [CreateWallMessageComponent]
})
export class CreateWallMessageModule {

	constructor() {}
}

