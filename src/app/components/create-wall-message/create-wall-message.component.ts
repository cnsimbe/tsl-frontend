import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { WallMessage } from "../../models"
import { LoadingController, AlertController } from "@ionic/angular"
import { ApiService } from "../../services/api.service"
import { finalize } from "rxjs/operators"
import { ImageCroppedEvent, ImageCropperComponent } from 'ngx-image-cropper';

@Component({
  selector: 'app-create-wall-message',
  templateUrl: './create-wall-message.component.html',
  styleUrls: ['./create-wall-message.component.scss']
})
export class CreateWallMessageComponent implements OnInit {

  constructor(private alertCtrl:AlertController, private loadCtrl:LoadingController, public apiService:ApiService) { }
  
  message = new WallMessage()
  onDone:Function;
  inputFile:File;
  @ViewChild('cropper') cropper : ImageCropperComponent;
  @ViewChild('fileElement') fileElement:ElementRef;

  
  ngOnInit() {

  }

  async onSubmit() {
  	if(this.message.text_message || this.message.picture)
  	{
  		let loader = await this.loadCtrl.create()
  		await loader.present()
  		this.apiService.createMessage(this.message)
  			.pipe(finalize(()=>loader.dismiss()))
  			.subscribe(wallMessage=>{
  				if(this.onDone)
  					this.onDone(wallMessage)
  			})
  	}
  	else
  		await (await this.alertCtrl.create({header:"Incomplete post",message:"Fill in the either the message or upload an image"})).present()
  }

  imageCropped($event:ImageCroppedEvent) {
  	this.message.picture = new File([$event.file], this.inputFile.name, this.inputFile )
  }

  removeImage() {
  	this.message.picture = undefined;
  	this.inputFile = undefined;
  	this.fileElement.nativeElement.value = ""
  }

}
