import { Component, OnInit, Input } from '@angular/core';
import { WallMessage } from "../../models"

@Component({
  selector: 'app-wall-message',
  templateUrl: './wall-message.component.html',
  styleUrls: ['./wall-message.component.scss']
})
export class WallMessageComponent implements OnInit {

  private _message:WallMessage;
  avatarURL:string;
  get message():WallMessage {
  	return this._message;
  }
  @Input('message') set message(m:WallMessage) {
  	this._message = m;
  	this.avatarURL = `https://ui-avatars.com/api/?name=${m.user.first_name}+${m.user.last_name}&rounded=true`
  }
  
  constructor() { }

  ngOnInit() {
  }

}
