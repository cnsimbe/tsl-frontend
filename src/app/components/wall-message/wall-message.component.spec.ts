
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ApiServiceMock } from "../../services/api.service.mock"
import { ApiService } from "../../services/api.service"
import { CommonModule } from '@angular/common';
import { IonicModule, IonImg } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { WallMessageComponent } from './wall-message.component';

describe('WallMessageComponent', () => {
  let component: WallMessageComponent;
  let parentElement: HTMLElement;
  let fixture: ComponentFixture<WallMessageComponent>;
  let wallMessage = { id: 1, picture:undefined, text_message: "Testing Message", created : "2018-09-09", user: { first_name: "Jane", last_name:"Doe", username:"jode", email:"j@doe.com" } }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, RouterTestingModule, FormsModule, IonicModule ],
      declarations: [ WallMessageComponent ],
      providers: [{ provide : ApiService, useValue: ApiServiceMock }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WallMessageComponent);
    component = fixture.componentInstance;
    component.message = wallMessage;
    parentElement = fixture.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set name correctly', () => {
    const titleElement = parentElement.querySelector('ion-card-subtitle')
    expect(titleElement.textContent).toEqual(`${wallMessage.user.first_name} ${wallMessage.user.last_name}`);
  });

  it('should not have message image', () => {
    const image = parentElement.querySelector('ion-img[picture]')
    expect(image).toBeFalsy()
  });

  it('should not have avatar image', () => {
    const image = parentElement.querySelector('ion-img[avatar]') as any
    expect(image).toBeTruthy()
    expect(image.src).toString()
  });

  it('should have message image', () => {
    let imageURL = "https://ui-avatars.com/api/?name=";
    wallMessage.picture = imageURL
    fixture.detectChanges()

    expect(component.message.picture).toEqual(imageURL)
    const image = parentElement.querySelector('ion-img[picture]')
    expect(image).toBeTruthy()
  });

});
