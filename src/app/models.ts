export class User {
	username:string;
	first_name:string;
	last_name:string;
	email:string;
	password?:string;
}

export class WallMessage {
	id:number;
	picture?:string|File|Blob;
	user:User;
	text_message?:string;
	created:string;
}

export class WallMessagePaginated {
	results:WallMessage[]
	count:number;
	previous:string;
	next:string;
}