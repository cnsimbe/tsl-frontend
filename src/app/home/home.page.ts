import { Component, OnInit } from '@angular/core';
import { ApiService } from "../services/api.service"
import { WallMessagePaginated } from "../models"
import { LoadingController, ModalController, AlertController } from "@ionic/angular"
import { finalize } from "rxjs/operators"
import { CreateWallMessageComponent } from "../components/create-wall-message/create-wall-message.component"

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
	
	paginatedResult = new WallMessagePaginated()
	currentPageURL:string;

	constructor(public api:ApiService, private alertCtrl:AlertController, public loadCtrl:LoadingController, public modalCtrl:ModalController) {}

	async ngOnInit() {
		return this.initialLoad();
	}

	async initialLoad() {
		let loader = await this.loadCtrl.create()
		await loader.present()
		this.api.getWallMessages()
			.pipe(finalize(()=>loader.dismiss()))
			.subscribe(results=>this.paginatedResult=results)
	}

	async loadPage(url:string) {

		let loader = await this.loadCtrl.create()
		await loader.present()
		this.api.getWallPageMessages(url)
			.pipe(finalize(()=>loader.dismiss()))
			.subscribe(results=>{
				this.currentPageURL = url;
				this.paginatedResult=results
			})
	}

	async logout() {

		let loader = await this.loadCtrl.create()
		await loader.present()
		this.api.logout()
			.pipe(finalize(()=>loader.dismiss()))
			.subscribe()
	}

	async createWallMessage() {

		if(!this.api.user)
			return await (await this.alertCtrl.create({header:"Login Required",message:"You must be logged in to create a wall message"})).present()

		let modal = await this.modalCtrl.create({component:CreateWallMessageComponent})
		let onDone = ()=>{
			if(this.currentPageURL)
				this.loadPage(this.currentPageURL)
			else
				this.initialLoad()
			modal.dismiss()
		}
		modal.componentProps = { onDone };
		await modal.present()
	}


}
