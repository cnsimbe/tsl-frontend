import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ApiServiceMock } from "../services/api.service.mock"
import { ApiService } from "../services/api.service"
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { LoadingController, ModalController, AlertController } from "@ionic/angular"

import { LoadingControllerSpy, AlertControllerSpy, ModalControllerSpy } from "../tests-ionic-mocks/mock-controllers"
import { WallMessageModule } from "../components/wall-message/wall-message.module"
import { CreateWallMessageModule } from "../components/create-wall-message/create-wall-message.module"
import { HomePage } from './home.page';
import { By } from '@angular/platform-browser';


describe('HomePage', () => {
  let component: HomePage;
  let fixture: ComponentFixture<HomePage>;
  let parentElement: HTMLElement;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, IonicModule.forRoot(), FormsModule, RouterTestingModule, WallMessageModule, CreateWallMessageModule],
      declarations: [ HomePage ],
      providers: [ { provide : ApiService, useValue: ApiServiceMock }, { provide: LoadingController, useValue: LoadingControllerSpy }, { provide: AlertController, useValue: AlertControllerSpy }, { provide:ModalController, useValue: ModalControllerSpy } ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePage);
    component = fixture.componentInstance;
    parentElement = fixture.nativeElement
    fixture.detectChanges();
  });
  
  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('should load wall messages', async () => {
    await component.ngOnInit()
    expect(component.paginatedResult.results).toBeTruthy()
  });


  it('should not show create wall message pop up', async () => {
    
    await component.createWallMessage()
    expect(component.modalCtrl.create).not.toHaveBeenCalled()
  });


  it('should show create wall message pop up', async () => {

    let user = { first_name: "Jane", last_name:"Doe", username:"jode", email:"j@doe.com" }
    component.api.user = user
    await component.createWallMessage()
    expect(component.modalCtrl.create).toHaveBeenCalled()
  });


  it('should load page url', async () => {

    let pageURL1 = 'https://testurl1'
    let pageURL2 = 'https://testurl2'
    
    await component.loadPage(pageURL1)
    expect(component.currentPageURL).toEqual(pageURL1)

    await component.loadPage(pageURL2)
    expect(component.currentPageURL).toEqual(pageURL2)
  });

  it('should call createWallMessage handler', async() => {
      
      spyOn(component, 'createWallMessage')

      let formElm =  fixture.debugElement.query(By.css('[createwall-btn]'))
      formElm.triggerEventHandler('click',null)
      await fixture.whenRenderingDone()
      await fixture.whenStable()

      expect(component.createWallMessage).toHaveBeenCalled()
      
  });

});
