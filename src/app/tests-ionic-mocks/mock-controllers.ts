declare var jasmine;

let loadCreate =  Promise.resolve({present:()=>Promise.resolve(),dismiss:()=>Promise.resolve()})
let LoadingControllerSpy = jasmine.createSpyObj('LoadingController', { create: loadCreate });
let AlertControllerSpy = jasmine.createSpyObj('AlertController', { create: loadCreate });
let ModalControllerSpy = jasmine.createSpyObj('ModalController', { create: loadCreate });

export { LoadingControllerSpy, AlertControllerSpy, ModalControllerSpy }