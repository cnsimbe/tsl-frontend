import { Component, OnInit, ViewChild,  ElementRef } from '@angular/core';
import { User } from "../models"
import { NgForm } from "@angular/forms"
import { Router } from "@angular/router"
import { AlertController, LoadingController } from "@ionic/angular"
import { ApiService } from "../services/api.service"
import { finalize, mergeMap } from "rxjs/operators"

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {

  @ViewChild('form') ngForm: NgForm;
  constructor(public router:Router,public apiService:ApiService,private alertCtrl:AlertController, private loadCtrl:LoadingController) { }
  user  = new User()

  ngOnInit() {
  }

  async onSubmit() {
  	if(this.user.email && this.user.first_name && this.user.last_name && this.user.username && this.user.password)
  	{
  		let loader = await this.loadCtrl.create()
  		await loader.present()
  		this.apiService.register(this.user)
        .pipe(mergeMap(()=>this.apiService.login(this.user.username,this.user.password)))
  			.pipe(finalize(()=>loader.dismiss()))
  			.pipe(finalize(()=>this.user=new User()))
  			.subscribe(async()=>{
          let alert = await this.alertCtrl.create({header:"Account created", message:"We have sent you a welcome email"})
          await alert.present()
          this.router.navigate(["/home"])
        })
  	}
  	else
  		await (await this.alertCtrl.create({header:"Form Error",message:"Fill in the required fields"})).present()

  }

}
