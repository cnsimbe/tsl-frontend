import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { SignupPage } from './signup.page';
import { HttpClientModule } from "@angular/common/http"
import { ApiServiceMock } from "../services/api.service.mock"
import { ApiService } from "../services/api.service"
import { CommonModule } from '@angular/common';
import { IonicModule, AlertController, LoadingController } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { AlertControllerSpy, LoadingControllerSpy } from "../tests-ionic-mocks/mock-controllers"

describe('SignupPage', () => {
  let component: SignupPage;
  let fixture: ComponentFixture<SignupPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule, ReactiveFormsModule, CommonModule, IonicModule, FormsModule, RouterTestingModule ],
      declarations: [ SignupPage ],
      providers: [ { provide : ApiService, useValue: ApiServiceMock }, { provide: AlertController, useValue: AlertControllerSpy }, { provide: LoadingController, useValue: LoadingControllerSpy } ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call submit form handler', async() => {
      
      spyOn(component, 'onSubmit')

      let formElm =  fixture.debugElement.query(By.css('form'))
      formElm.triggerEventHandler('submit',null)
      await fixture.whenRenderingDone()
      await fixture.whenStable()

      expect(component.onSubmit).toHaveBeenCalled()
      
  });


  it('should not attempt register', async () => {

    fixture.detectChanges()
    await component.onSubmit()
    expect(component.apiService.register).not.toHaveBeenCalled()
    
  });

  it('should attempt register', async () => {

    component.user =  { first_name: "Jane", last_name:"Doe", username:"jode", email:"j@doe.com", password:"password" }
    fixture.detectChanges()

    spyOn(component.router,'navigate')

    await component.onSubmit()
    expect(component.apiService.register).toHaveBeenCalled()
    expect(component.router.navigate).toHaveBeenCalled()
    
  });

});
