import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from "@angular/forms"
import { Router } from "@angular/router"
import { AlertController, LoadingController } from "@ionic/angular"
import { ApiService } from "../services/api.service"
import { finalize } from "rxjs/operators"

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(private alertCtrl:AlertController, public apiService:ApiService, public router:Router, private loadCtrl:LoadingController) { }
  username:string;
  password:string;
  ngOnInit() {
  }


  async onSubmit() {
    
  	if(this.username && this.password)
  	{
  		let loader = await this.loadCtrl.create()
  		await loader.present()
  		this.apiService.login(this.username,this.password)
  			.pipe(finalize(()=>loader.dismiss()))
  			.pipe(finalize(()=>this.password=undefined))
  			.subscribe(()=>this.router.navigate(["/home"]))
  	}
  	else
  		await (await this.alertCtrl.create({header:"Form Error",message:"Fill in the required fields"})).present()

  }

}
