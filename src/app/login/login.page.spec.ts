import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from "@angular/common/http/testing"
import { ApiServiceMock } from "../services/api.service.mock"
import { ApiService } from "../services/api.service"
import { CommonModule } from '@angular/common';
import { IonicModule, AlertController, LoadingController } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginPage } from './login.page';
import { By } from '@angular/platform-browser';
import { of } from "rxjs"
import { delay } from "rxjs/operators"
import { AlertControllerSpy, LoadingControllerSpy } from "../tests-ionic-mocks/mock-controllers"

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let parentElement: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule, IonicModule, FormsModule, RouterTestingModule ],
      declarations: [ LoginPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [ { provide : ApiService, useValue: ApiServiceMock }, { provide: AlertController, useValue: AlertControllerSpy }, { provide: LoadingController, useValue: LoadingControllerSpy } ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    
  });


  it('should call submit form handler', async() => {
      
      spyOn(component, 'onSubmit')

      let formElm =  fixture.debugElement.query(By.css('form'))
      formElm.triggerEventHandler('submit',null)
      await fixture.whenRenderingDone()
      await fixture.whenStable()

      expect(component.onSubmit).toHaveBeenCalled()
      
  });


  it('should not attempt login', async () => {

    fixture.detectChanges()
    await component.onSubmit()
    expect(component.apiService.login).not.toHaveBeenCalled()
    
  });

  it('should attempt login', async () => {

    component.username = "username"
    component.password = "password"
    fixture.detectChanges()

    spyOn(component.router,'navigate')

    await component.onSubmit()
    expect(component.apiService.login).toHaveBeenCalled()
    expect(component.router.navigate).toHaveBeenCalled()
    
  });

});
